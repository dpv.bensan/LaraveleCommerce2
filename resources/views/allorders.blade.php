@extends('templates.template')

@section('title', 'Cart')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped border">
				<thead>
					<tr>
						<th>ID</th>
						<th>customer NAME</th>
						<th>customer Email</th>
						<th>DATE</th>
						<th>DETAILS</th>
						<th>TOTAL</th>
						<th>PAYMENT</th>
						<th>STATUS</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
					<tr>
						
						<td>{{$order->created_at->format('U')}}{{$order->id}}</td>
						<td>
							{{$order->user->name}}
						</td>
						<td>{{$order->created_at->diffForHumans()}}</td>
						<td>
							@foreach($order->items as $item)
							name: {{$item->name}},
							Qty: {{$item->pivot->quantity}}
							<br>
							@endforeach
						</td>
						<td>{{$order->total}}</td>
						<td>{{$order->payment->name}}</td>
						<td>{{$order->status->name}}</td>
						<td>
						
							@if($order->status_id == 1)
							<form action="/admin/cancelorder/{{$order->id}}" method="POST">
								@csrf
								@method('PATCH')
								<button class="btn btn-danger" type="SUBMIT">VOID</button>
							</form>
								<form action="/admin/markaspaid/{{$order->id}}" method="POST">
									@csrf
									@method('PATCH')
									<button class="btn btn-primary" type="SUBMIT">PAID</button>
								</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
				
			</table>
		</div>
	</div>
</div>

@endsection