<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>

	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	  <a class="navbar-brand" href="/">B46</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarColor01">
	    <ul class="navbar-nav mr-auto">
	    	<li class="nav-item active">
		      <a class="nav-link" href="/catalog">Catalog<span class="sr-only">(current)</span></a>
		    </li>
	   
	      @auth
	      		<li class="nav-item">
	        	<a class="nav-link" href="#">Welcome: {{auth::user()->name}}</a>
	      	</li>
		    <li class="nav-item">
		    	<form action="/logout" method="POST">
		    		@csrf
		    		<button type="submit" class="btn btn-secondary">Logout</button>
		    	</form>
		    </li>
		    @if(Auth::user()->role_id==1)
			    <li class="nav-item">
			        <a class="nav-link" href="/admin/allorders">All Orders</a>
			    </li>
			    <li class="nav-item active">
		       		<a class="nav-link" href="/admin/add-item">Add Item<span class="sr-only">(current)</span></a>
		     	 </li>
		     	 <li class="nav-item active">
		       		<a class="nav-link" href="/admin/users">Users</span></a>
		    	 </li>
		    @else
			    <li class="nav-item">
		        	<a class="nav-link" href="/cart">Show Cart <span class="badge bg-dark" id="cartCount">
		        		@if(Session::has('cart')){{array_sum(Session::get('cart'))}}
    					@else
    					0
    					@endif
		        	</span></a>
		      	</li>
		      	<li class="nav-item">
		        	<a class="nav-link" href="/orders">My Orders</a>
		      	</li>
		     @endif
	      @else
	     		<li class="nav-item">
		        	<a class="nav-link" href="/register">Register</a>
		      	</li>
		      	<li class="nav-item">
		        	<a class="nav-link" href="/login">Login</a>
		      	</li>
	      @endauth
	    </ul>
	  </div>
	</nav>

	@yield('content')

	<footer class="footer bg-dark">
		<div class="container">
			<p class="text-center text-white">
				Made with Love By: Airy TEHHEEEEE
			</p>
		</div>
	</footer>

</body>
</html>