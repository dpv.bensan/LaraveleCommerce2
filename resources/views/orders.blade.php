@extends('templates.template')

@section('title', 'Cart')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped border">
				<thead>
					<tr>
						<th>ID</th>
						<th>DATE</th>
						<th>DETAILS</th>
						<th>TOTAL</th>
						<th>PAYMENT</th>
						<th>STATUS</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
					<tr>
						
						<td>{{$order->created_at->format('U')}}{{$order->id}}</td>
						<td>{{$order->created_at->diffForHumans()}}</td>
						<td>
							@foreach($order->items as $item)
							name: {{$item->name}},
							Qty: {{$item->pivot->quantity}}
							<br>
							@endforeach
						</td>
						<td>{{$order->total}}</td>
						<td>{{$order->payment->name}}</td>
						<td>{{$order->status->name}}</td>

					</tr>
					@endforeach
				</tbody>
				
			</table>
		</div>
	</div>
</div>

@endsection