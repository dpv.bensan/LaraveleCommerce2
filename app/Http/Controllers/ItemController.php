<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use Session;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items  = Item::all();
        return view('catalog', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('add-item-form', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'img_path' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048',
            'category_id' => 'required'
        ];

        $this->validate($request, $rules);


        $item = new Item;
        $item->name = $request->get('name');
        $item->description = $request->get('description');
        $item->price = $request->get('price');
        $item->category_id = $request->get('category_id');

        $image = $request->file('img_path');
        $image_name = time().".".$image->getClientOriginalExtension();

        $destination = 'images/';
        $image->move($destination, $image_name);

        $item->img_path = $destination.$image_name;

        $item->save();

        Session::flash("message", "$item->name has been added");

        return redirect('/catalog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $categories = Category::all();
        return view('edit-item-form', compact('item', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $item = Item::find($id);

        $rules = [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'img_path' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048',
            'category_id' => 'required'
        ];

        $this->validate($request, $rules);

        $item->name = $request->get('name');
        $item->description = $request->get('description');
        $item->price = $request->get('price');
        $item->category_id = $request->get('category_id');

        if($request->file('img_path') != null) {
            $image = $request->file('img_path');
            $image_name = time().".".$image->getClientOriginalExtension();

            $destination = 'images/';
            $image->move($destination, $image_name);

            $item->img_path = $destination.$image_name;
        }

        $item->save();

        Session::flash("message", "$item->name has been updated");

        return redirect('/catalog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $item = Item::find($request->get('id'));
        $item->delete();
        Session::flash("message", "$item->name has been deleted");
        return redirect('/catalog');
    }

    public function showCart() {

        $item_cart = [];
        $total = 0;

        if (Session::has('cart')) {
            $cart = Session::get('cart');
            foreach ($cart as $id => $quantity) {
                $item = Item::find($id);
                $item->quantity = $quantity;
                $item->subtotal = $item->price * $quantity;
                $item_cart[] = $item;
                $total += $item->subtotal;
            }
        }

        return view('cart', compact('item_cart', 'total'));
    }

    public function addToCart($id, Request $request)
    {
        if(Session::has('cart')) {
            $cart = Session::get('cart');
        } else {
            $cart = [];
        }

        if (isset($cart[$id])) {
            $cart[$id] += $request->get('quantity');
        } else {
            $cart[$id] = $request->get('quantity');
        }

        Session::put('cart', $cart);

        $item = Item::find($id);
        Session::flash('message', "$request->quantity of $item->name successfully added to cart");
        
        return array_sum($cart); 
    }

    public function changeQty($id, $quantity) {
        Session::forget("cart.$id");
        return redirect('/cart');
    }

    public function emptyCart()
    {
        Session::forget('cart');
        return redirect('/cart');
    }

    public function updateQty(Request $request)
    {
        $cart = Session::get('cart');
        $cart[$request->id] = $request->get('quantity');
        Session::put('cart', $cart);
        return redirect('/cart');
    }
}
