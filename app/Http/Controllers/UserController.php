<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserController extends Controller
{
    public function index()
    {
    	$users = User::all();
    	$roles = Role::all();
    	return view('users', compact('users', 'roles'));
    }

    public function updateRole($id, Request $request)
    {
    	$user = User::find($id);
    	$user->role_id = $request->get('role_id');
    	$user->save();
    	return redirect('/admin/users');
    }
}
